const menuConfig = {
	title: 'Foo',
	body: 'Bar',
	buttonText: 'Baz',
	cancellable: true
}

function createMenu(menuConfig) {

}

// Functions so do one thing
function emailClients(clients) {
	clients.forEach((client) => {
		const clientRecord = database.lookup(client);
		if (clientRecord.isActive) {
			email(client)
		}
	})
}

// Function names should say what they do
function addToDate(mont, date) {
	// ...
}

const date = new Date()
addToDate(1, date)

// Functions should only be one level of abstraction

// Dont use flags as functions parameters

// Bad
function createFile(name) {
	if (temp) {
		fs.create(`./temp/${name}`)
	}
	else {
		fs.create(name)
	}
}

// Good
function createFile(name) {
	fs.create(`./temp/${name}`)
}

function createTempFile(name) {
	fs.create(name)
}

//--------------------------------

// Avoid side effects

// Bad
/*
let name = 'Darwin Manalo'
function splitIntoArray() {
	name = name.split(' ')
}

splitIntoArray()
console.log(name)
*/

// Good
const name = 'Darwin Manalo'

function splitIntoArray(name) {
	return name.split(' ')
}
const newName = splitIntoArray(name)

console.log(name)
console.log(newName)

//------------------------------

const programmers = [
	{
		name: 'Darwin',
		language: ['PHP', 'JavaScript', 'Java']
	},
	{
		name: 'James',
		language: 'PHP'
	},
	{
		name: 'John',
		language: 'JavaScript'
	},
	{
		name: 'Helen',
		language: ['Python', 'C', 'C++']
	}
]

programmers.map(programmer => console.log(programmer.name))
programmers.filter(programmer => {
	if (programmer.language == 'PHP') {
		// console.log(`${programmer.name} knows PHP`)
	}
})

programmers.filter(programmer => {
	const { name, language } = programmer
	// console.log(`${name} knows ${language}`)
})

//---------------------------------------

const active = true 
function isActive() {
	active && console.log('He is active')	
}

isActive()
