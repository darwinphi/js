// SOLID

// Single Responsibility Method
// Bad
/*
class UserSettings {
	constructor(user) {

	}

	changeSettings(settings) {
		if (this.verifyCredentials) {

		}
	}

	verifyCredentials() {

	}
}
*/

// Good
class UserAuth {
 	constructor(user) {
 		this.user = user
 	}

 	verifyCredentials() {
 		// ...
 	}
}

class UserSettings {
 	constructor(user) {
 		this.user = user
 		this.auth = new UserAuth(user)
 	}

 	changeSettings(settings) {
 		if (this.auth.verifyCredentials()) {

 		}
 	}
}

// Open / Close


class iceCreamMaker {
	constructor() {
		this.flavors = ['chocolate']
	}

	makeIceCream(flavor = this.flavors) {
		console.log(`Yehey icecream ${flavor}`)
	}
}

const choco = new iceCreamMaker
choco.makeIceCream()
choco.makeIceCream('Vanilla')

// Liskov

class Shape {
	setColor(color) {

	}

	render(area) {
		console.log(area)
	}
}

class Rectangle extends Shape {
	constructor(width, height) {
		super()
		this.width = width 
		this.height = height
	}

	getArea() {
		return this.width * this.height
	}
}

class Square extends Shape {
	constructor(length) {
		super()
		this.length = length
	}

	getArea() {
		return this.length * this.length
	}
}

function renderShape(shapes) {
	shapes.forEach((shape) => {
		const area = shape.getArea()
		shape.render(area)
	})
}

const shapes = [new Rectangle(4, 7), new Square(4)]
renderShape(shapes)