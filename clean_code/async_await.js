/**
 * XHR
 * @return {promise}
 */
// function getJSON() {
// 	return new Promise((resolve, reject) => {
// 		const xhr = new XMLHttpRequest()
// 		xhr.open("GET", "http://jsonplaceholder.typicode.com/users")
// 		xhr.onload = () => resolve(JSON.parse(xhr.responseText))
// 		xhr.onerror = () => reject(xhr.statusText)
// 		xhr.send()	
// 	})
// }

/**
 * Fetch API function
 * @return {promise}
 */
function getJSON() {
	return fetch('./data.json')
	// return fetch('http://jsonplaceholder.typicode.com/users')
}

function printName(response) {
	response.map(data => console.log(`Name: ${data.name}`))
}

function greet() {
	console.log('Hello, World')
}

const getData = async() => {
	try {
		const response = await getJSON()
		const json = await response.json()
		await greet()
		await printName(json)
	}
	catch(err) {
		console.log(err)
	}
}

getData()