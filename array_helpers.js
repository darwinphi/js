const people = ['Darwin, Manalo', 'John, Cruz', 'Helen, Dy', 'Anne, Pineda', 'Magnus, Carlsen']

const inventors = [
	{ first: 'Darwin', last: 'Manalo', year: 1996, passed: 2067},
	{ first: 'John', last: 'Cruz', year: 1988, passed: 2054},
	{ first: 'Helen', last: 'Dy', year: 1981, passed: 2032},
	{ first: 'Anne', last: 'Pineda', year: 1994, passed: 2022},
	{ first: 'Magnus', last: 'Carlsen', year: 1993, passed: 2070}
]

// Array.prototype.filter()
// 
const eighteen = inventors.filter(function(inventor){
	if (inventor.year >= 1980 && inventor.year < 1990) {
		return true;
	}
})
console.log('---FILTER()---')
console.log(eighteen)

const nineteen = inventors.filter(inventor => inventor.year >= 1990 && inventor.year < 2000)

console.log(nineteen)

// Array.prototype.map()
const fullNames = inventors.map(inventor => `${inventor.first} ${inventor.last}`)
console.log('---MAP()---')
console.log(fullNames)

// Array.prototype.sort()
const ordered = inventors.sort((a, b) => a.year > b.year ? 1 : -1)
console.log('---SORT()---')
console.log(ordered)

const alpha = people.sort(function(lastOne, nextOne) {
	const [aFirst, aLast] = lastOne.split(', ')
	const [bFirst, bLast] = nextOne.split(', ')
	return aLast > bLast ? 1 : -1
})

console.log(alpha)

// Array.prototype.reduce()
const totalYears = inventors.reduce((total, inventor) => total + (inventor.passed - inventor.year), 0)
console.log('---REDUCE---')
console.log(totalYears)

// Sort the inventor by years lived
const oldest = inventors.sort(function(a, b) {
	const lastGuy = a.passed - a.year
	const nextGuy = b.passed - b.year
	return lastGuy > nextGuy ? 1 : -1
})

console.log('---SORT OLDEST---')
console.log(oldest)

const data = ['cat', 'car', 'truck', 'truck', 'bike', 'motor', 'walk', 'cat', 'bat']
const transpo = data.reduce(function(obj, item) {
	// console.log(item)
	if (!obj[item]) {
		obj[item] = 0
	}
	obj[item]++
	return obj
}, {})

console.log(transpo)
