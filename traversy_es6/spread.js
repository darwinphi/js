let args1 = [1, 2, 3]
let args2 = [4, 5, 6]

function spread() {
	console.log(`${args1},${args2}`)
}

spread(...args1, ...args2)