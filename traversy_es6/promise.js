
// Immediately Resolved
// var promise = Promise.resolve('Foo')

// promise.then(function(response) {
// 	console.log(response)
// })

// var promise = new Promise(function(resolve, reject) {
// 	setTimeout(function() {
// 		return resolve(4)
// 	}, 1000)
// })

// promise.then(function(resolve) {
// 	console.log(resolve += 3)
// })

function getData(method, url) {
	return new Promise(function(resolve, reject) {
		var xhr = new XMLHttpRequest()
		xhr.open(method, url)
		xhr.onload = function() {
			if (this.status >= 200 && this.status < 300) {
				resolve(xhr.response)
			}
			else {
				reject({
					status: this.status,
					statusText: xhr.statusText
				})
			}
		}

		xhr.onerror = function() {
			reject({
				status: this.status,
				statusText: xhr.statusText
			})
		}

		xhr.send()
	})
}

getData('GET', 'http://jsonplaceholder.typicode.com/todos').then(function(data) {
	// console.log(data)
	let todos = JSON.parse(data)
	let output = ''
	for (let todo of todos) {
		output += `
			<li>${todo.title}</li>
		`
	}

	document.getElementById('template').innerHTML = output

}).catch(function(err) {
	console.log(err)
})