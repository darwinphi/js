let string = 'Hello World, How are you Helen'

// Returns true or false
console.log(string.startsWith('Hello')) // "true"
console.log(string.endsWith('Helen')) // "true"
console.log(string.includes('you')) // "true"