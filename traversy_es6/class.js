class User {
	constructor(name, age) {
		this.name = name
		this.age = age 
	}
	
	static countUsers() {
		console.log('There are 50 users')
	}

	greet() {
		console.log(`Hello ${this.name}. You are ${this.age} years old`)
	}
}

let darwin = new User('darwin', 21)
darwin.greet()

// -- Call a static method --
User.countUsers()


// -- Inheritance --
class Member extends User {
	constructor(name, age, memberPackage) {
		super(name, age)
		this.package = memberPackage
	}

	getPackage() {
		console.log(`Hello ${this.name}. Your Package: ${this.package}`)
	}
}

let helen = new Member('Helen', 26, 'Standard')

helen.getPackage()
helen.greet()