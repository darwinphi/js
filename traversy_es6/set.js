let myArray = [1, 2, 3, 4, 5]
let mySet = new Set(myArray)

mySet.add('20')
mySet.add({a: 21, b: 22})
mySet.delete(1)
// mySet.clear()

console.log(mySet)
console.log(mySet.size)

mySet.forEach(function(value) {
	console.log(value)
})