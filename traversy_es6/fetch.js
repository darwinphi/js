const url = 'http://jsonplaceholder.typicode.com/users'
let users = []

// fetch(url)
// 	.then(function(blob) {
// 		return blob.json()
// 	})
// 	.then(function(data) {
// 		users.push(...data)
// 		// for (var i = 0; i < users.length; i++) {
// 		// 	console.log(users[i].name)
// 		// }

// 		// users.map(function(user) {
// 		// 	console.log(user.name)
// 		// })
		
// 		let output = ''
// 		for (let user of users) {
// 			output += `
// 				<li>${user.name}</li>
// 			`
// 		}

// 		document.getElementById('js-users').innerHTML = output

// 	})

fetch(url)
	.then(blob => blob.json())
	.then(data => {
		users.push(...data)
		let output = ''
		// for (let user of users) {
		// 	output += `<li>${user.name}</li>`
		// }
		let lists = users.map(user => `
			<li>${user.name}, ${user.username}</li>
		`).join('')
		document.getElementById('js-users').innerHTML = lists
	})
