let animal = {
	species: 'dog',
	weight: 21,
	sound: 'Woof'
}

let {species, sound } = animal
// let species = animal.species
// let sound = animal.sound

console.log(`The dog says ${sound}`)

makeSound({
	weight: 21,
	sound: 'Woof'
})

// function makeSound(options) {
// 	options.species = options.species || 'animal'
// 	console.log(`The ${options.species} says ${options.sound}`)
// }

// function makeSound(options) {
// 	let { weight, sound } = options
// 	let species = options.species || 'animal'
// 	console.log(`The ${weight}-pound ${species} says ${sound}`)
// }

function makeSound({species = 'animal', weight, sound}) {
	console.log(`The ${weight}-pound ${species} says ${sound}`)
}