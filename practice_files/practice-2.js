var student = {
	name: 'Darwin Manalo',
	greet: function() {
		console.log(this.name)
	}
}

var person = (function() {
	var view = {
		greetings: function(name) {
			console.log(`Hello, ${name}`)
		}
	}
	return {
		greet: function(name) {
			return view.greetings(name)
		}
	}
})()

person.greet('Darwin')
// student.greet()

//-------------------------------------

function Person(name, age) {
	this.name = name 
	this.age = age 

	this.info = function() {
		console.log(`Hi ${this.name}. Your age is ${this.age}`)
	}
}

let darwin = new Person('Darwin', 21)

darwin.info()

var Module = (function () {

  // locally scoped Object
  var myObject = {};

  // declared with `var`, must be "private"
  var privateMethod = function () {
  	console.log("Yow")
  };

  myObject.someMethod = function () {
    return privateMethod()
  };
  
  return myObject;

})();

Module.someMethod()


