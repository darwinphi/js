// const button = document.getElementById('js-submit')

// button.addEventListener('click', (function() {
// 	let count = 0

// 	return function() {
// 		count += 1

// 		if (count === 2) {
// 			alert('Every 2 click')
// 			count = 0
// 		}
// 	}
// })())

function Hero(name, role) {
	this.name = name 
	this.role = role
	this.introduce = function() {
		console.log(`My name is ${this.name} and I'm a ${this.role}`)
	}
}

const darwin = new Hero('Darwin', 'Student')
darwin.introduce()

const dog = {
	name: "Jeho"
}

function greet(message) {
	console.log(`${message}, ${this.name}`)
}

greet.call(dog, "Hello cutie") // Hello cutie, Jeho
greet.apply(dog, ["Hi cutie"])

const names = ['Darwin', 'Helen', 'Jane']

console.log(names.join(' '));