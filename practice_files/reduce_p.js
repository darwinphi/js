let numbers = [22, 44, 11, 43, 67]
// let sum = 0

// for (var i = 0; i < numbers.length; i++) {
// 	sum += numbers[i]
// }

// for (let number of numbers) {
// 	sum += number
// }

function sum(acc, val) {
	console.log(acc)
	return acc + val
}

function findMax(acc, val) {
	return acc > val ? acc : val
}

let biggest = numbers.reduce(findMax)
console.log(biggest)

// let answer = numbers.reduce(sum, 0)
// console.log(answer)

let even = numbers.filter((num) => (num % 2 == 0))
console.log(even)
// console.log(sum)