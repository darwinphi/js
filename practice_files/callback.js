function greet(callback) {
	if (callback instanceof Function) {
		callback()
	}
}

function sayHello() {
	console.log("Say Hello")
}

// Passing a named function
greet(sayHello)

// Passing an anonymous function
greet(function() {
	console.log("Hello World")
})

function multiplier(factor) {
	// return function(x) {
	// 	return x * factor
	// }
	return x => x * factor
}

let doubler = multiplier(2)
let tripler = multiplier(3)

// console.log(doubler)
// `doubler` is now equivalent to
// f (x) {
// 	return x * factor
// }

console.log(doubler(3))

var names = ['Helen', 'Darwin']

let numbers = [1, 2, 3, 4, 5]

function doubler2(x) {
	return x * 2
}

let doubled = numbers.map(doubler2)

console.log(numbers)
console.log(doubled)


vals = Array(100).fill().map(Math.random)
console.log(vals)

