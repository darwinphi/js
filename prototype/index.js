// Prototypical Inheritance

function Greet(name) {
	this.name = name
}

Greet.prototype.print = function() {
	console.log(this.name)
}

let newUser = new Greet('Darwin')

newUser.print() 

// Classical Inheritance

class Greeting {
	constructor(name) {
		this.name = name
	}

	print() {
		console.log(`Hello ${this.name}`)
	}

}

let darwin = new Greeting('Darwin')

darwin.print()

class Question extends Greeting {
	constructor(name, q) {
		super(name)
		this.q = q
	}

	print() {
		super.print()
		console.log(`Hello ${this.name}. ${this.q}`)
	}
}

let question = new Question('darwin', 'how are you?')
question.print()

// 

const obj = {
	message: "They can be added to objects like variables",
	log(message) {
		console.log(message)
	}
}

obj.log(obj.message)

// 


var createScream = function(logger) {
	return function(message) {
		logger(message.toUpperCase() + "!!!")
	}
}

const scream = createScream(message => console.log(message))
scream('functions can be returned from other functions')
scream('createScream returns a function')
scream('scream invokes that returned function')

// Imperative vs Declarative

// IMPERATIVE

var string = "This is the mid day show with Cheryl Waters";
var urlFriendly = "";
for (var i=0; i<string.length; i++) {
	if (string[i] === " ") {
		urlFriendly += "-";
	} 
	else {
		urlFriendly += string[i];
	}
}
console.log(urlFriendly);

// DECLARATIVE
const string2 = "This is the mid day show with Cheryl Waters"
const urlFriendly2 = string.replace(/ /g, "-")
console.log(urlFriendly2)
