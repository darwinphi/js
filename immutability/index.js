let color_lawn = {
	title: "lawn",
	color: "#00FF00",
	rating: 0
}

// Mutable

// function rateColor(color, rating) {
// color.rating = rating
// return color
// }

// console.log(rateColor(color_lawn, 5).rating) // 5
// console.log(color_lawn.rating) // 5

// IMMUTABLE--------------------

// var rateColor = function(color, rating) {
// return Object.assign({}, color, {rating:rating})
// }

// OR 

const rateColor = (color, rating) =>
({
	...color,
	rating
})


console.log(rateColor(color_lawn, 5).rating) // 5
console.log(color_lawn.rating) // 0

// ----------------------------

// MUTABLE - using array.push is not immutable
let list = [
	{ title: "Rad Red"},
	{ title: "Lawn"},
	{ title: "Party Pink"}
]

// var addColor = function(title, colors) {
// 	colors.push({ title: title })
// 	return colors;
// }

// console.log(addColor("Glam Green", list).length) // 4
// console.log(list.length) // 4

// IMMUTABLE - using array.concat

const addColor = (title, array) => array.concat({title})
console.log(addColor("Glam Green", list).length) // 4
console.log(list.length) // 3
