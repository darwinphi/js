// Impure function -------------------
/* Why? It does not return a value or a function. It also changes a variable outside of its scope: frederick. Once the selfEducate function is invoked, something about the “world” has changed. It causes side effects */

const frederick = {
	name: "Frederick Douglass",
	canRead: false,
	canWrite: false
}
const selfEducate = (person) => {
	person.canRead = true
	person.canWrite = true
	return person
}

console.log( selfEducate(frederick) )
console.log( frederick )

// {name: "Frederick Douglass", canRead: false, canWrite: false}
// {name: "Frederick Douglass", canRead: false, canWrite: false}


// Pure function -----------------------
/* Why? It computes a value based
on the argument that was sent to it - the person. It returns a new person object
without mutating the argument sent to it and therefore has no side effects. */

const frederick2 = {
	name: "Frederick Douglass",
	canRead: false,
	canWrite: false
}

// const selfEducate2 = person =>
// 	({
// 		...person,
// 		canRead: true,
// 		canWrite: true
// 	})

const selfEducate2 = function(person){
	return ({
		...person,
		canRead: true,
		canWrite: true
	})
}
console.log( selfEducate2(frederick2) )
console.log( frederick2 );

// {name: "Frederick Douglass", canRead: true, canWrite: true}
// {name: "Frederick Douglass", canRead: false, canWrite: false}

var card = function(){
  greet = 'Darwin'

  function getAge(age) {
  	return age
  }

  return {
  	greet: greet,
  	age: getAge
  }
}()

console.log(card.greet)
console.log(card.age(1))