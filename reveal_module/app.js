const App = (function() {
	
	let data = {
		lists: []
	}

	const controller = {
		addTodo(title) {
			data.lists.push({title: title})
			
			view.render()
		},
		removeTodo(title) {
			let removeTitle = data.lists.map(list => list.title).indexOf(title)
			dataList().splice(removeTitle, 1)

			view.render()
		}
	}

	const view = {

		init: function() {
			let addTitle = document.getElementById('js-add')
			
			addTitle.addEventListener("click", function() {
				let title = document.getElementById('js-title').value
				const notEmpty = title.length > 0 ? true : false
				notEmpty ? controller.addTodo(title) : alert('Please enter a value')
			})

			this.render()
		},

		render: function() {
			console.log(data.lists)	
		}
	}
 
	return {
		init: function() {
			return view.init()
		}
	}
})()

App.init()