let getTextBtn = document.getElementById('js-getText')
let output = document.getElementById('js-output')

getTextBtn.addEventListener('click', getText)

function getText() {
   fetch('sample.txt')
      .then(response => getResponse(response))
}

function getResponse(response) {
   if (response.ok) {
      console.log(response)
   }
}

