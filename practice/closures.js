// -- Closures --
// JS functions are closure which means the greet function can access the outer scope
// In other language you should pass an argument to the function

var name = 'Darwin'

function greet() {
	console.log(`Hello ${name}`)
}

greet()

var notes = function() {
	var xhr = new XMLHttpRequest()

	xhr.open('GET', 'sample.txt', true)

	xhr.onload = function() {
		if (this.status == 200) {
			console.log('Hello')
		}
	}

	xhr.send()
}

notes()
