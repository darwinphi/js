let names = ['Darwin', 'Helen', 'John', 'Jane']
let students = names.map(name => `<li> Your name is ${name} </li>`).join("")
let list = document.getElementById('js-list')
list.innerHTML = students


// let people = [
// 	{ name: 'Darwin', age: 21 },
// 	{ name: 'Helen', age: 26 },
// 	{ name: 'Nikka', age: 22 }
// ]

// let names = people.filter(pip => (pip.age < 22 ) ? `<li> ${pip.name} </li>` : '')

// console.log(names)


let orders = [
	{ amount: 150 },
	{ amount: 250 },
	{ amount: 350 },
	{ amount: 275 },
	{ amount: 175 }
]

// 0 is the starting point for sum
// Iterated item is second argument
let totalAmount = orders.reduce(function(sum, order){
	console.log(sum, order)
	return sum += order.amount
}, 0)

console.log(totalAmount)
