
// Sets a key and value pair
localStorage.setItem('breakfast', 'cereal')
console.log(localStorage.getItem('breakfast'))

// Remove specific key
localStorage.removeItem('breakfast')

// Remove all items
// localStorage.clear()

document.cookie = "username=true";

console.log(document.cookie)

// sessionStorage.setItem('breakfast', 'eggs')
// console.log(sessionStorage.getItem('breakfast'))